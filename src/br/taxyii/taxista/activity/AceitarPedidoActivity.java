package br.taxyii.taxista.activity;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import br.taxyii.library.fragment.AceitarCorridaFragment;
import br.taxyii.taxista.R;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

public class AceitarPedidoActivity extends SherlockFragmentActivity {

	private AceitarCorridaFragment fragAceitarCorrida;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_aceitar_pedido);
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		
		fragAceitarCorrida = (AceitarCorridaFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentAceitarCorrida);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
