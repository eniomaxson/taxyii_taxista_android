package br.taxyii.taxista.activity;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckedTextView;
import android.widget.Toast;
import br.taxyii.library.adapter.FormaPagamentoAdapter;
import br.taxyii.library.dao.UsuarioDAO;
import br.taxyii.library.faces.ITransacao;
import br.taxyii.library.model.FormaPagamentoModel;
import br.taxyii.library.model.UsuarioModel;
import br.taxyii.library.task.DefaultTask;
import br.taxyii.library.utils.AndroidUtils;
import br.taxyii.library.utils.GcmUtils;
import br.taxyii.taxista.R;

import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class AdicionarPagamentoActivity extends SherlockListActivity implements
		ITransacao, OnItemClickListener {
	private String response;
	private ArrayList<FormaPagamentoModel> formasPagamento;
	private FormaPagamentoModel formaPagamento;
	private FormaPagamentoAdapter formasPagamentoAdapter;
	private DefaultTask task;
	private UsuarioModel usuario;
	private int SALVAR = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);

		formaPagamento = new FormaPagamentoModel();
		
		usuario = new UsuarioDAO(getBaseContext()).get();
		
		task = new DefaultTask(this, this, getString(R.string.tituloProgressAdicionarPagamento),getString(R.string.mensagemProgressListarFormaPagamento),0);
		task.execute();
		
		getListView().setOnItemClickListener(this);
	}
	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(0, SALVAR, 0, "Salvar")
				.setIcon(R.drawable.abs__ic_cab_done_holo_dark)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int opcao = item.getItemId();

		if (android.R.id.home == opcao) {
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}

		if (opcao == SALVAR) {
			task = new DefaultTask(this, new TAddFormaPagamento(),getString(R.string.tituloProgressAdicionarPagamento),getString(R.string.mensagemProgressAdicionarPagamento), 0);
			task.execute();
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void executar() throws Exception {
		formaPagamento.setUrl("formaspagamento/" + usuario.getId(), GcmUtils.SENDER_ID_MOTORISTA);
		response = formaPagamento.requisicaoHttp("get");
	}

	@SuppressWarnings("unchecked")
	@Override
	public void atualizarView() {
		if (response != null) {
			try{
				JSONObject json = new JSONObject(response);
				if(!json.has("error")){
					formasPagamento = (ArrayList<FormaPagamentoModel>) new FormaPagamentoModel().toCollectionObject(new JSONArray(json.getString("data")));
					formasPagamentoAdapter = new FormaPagamentoAdapter(getBaseContext(), formasPagamento);
					getListView().setAdapter(formasPagamentoAdapter);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}else{
			AndroidUtils.showSimpleToast(this, R.string.error_conexao, Toast.LENGTH_LONG);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position,long arg3) {
		CheckedTextView checked = (CheckedTextView) v;

		if (checked.isChecked()) {
			formasPagamento.get(position).setAceita(0);
			checked.setChecked(false);
		} else {
			checked.setChecked(true);
			formasPagamento.get(position).setAceita(1);
		}

	}

	private class TAddFormaPagamento implements ITransacao {

		@Override
		public void executar() throws Exception {
			response = new FormaPagamentoModel().atualizaFormaPagamento(getBaseContext(), formasPagamento);
		}

		@Override
		public void atualizarView() {
			if (response != null) {
				try {
					JSONObject json= new JSONObject(response);
					if (!json.has("error")) {
						AndroidUtils.showSimpleToast(AdicionarPagamentoActivity.this,R.string.frmAdicionadoSucesso, Toast.LENGTH_SHORT);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else {
				AndroidUtils.showSimpleToast(AdicionarPagamentoActivity.this,R.string.error_conexao, Toast.LENGTH_SHORT);
			}
		}
	}

}
