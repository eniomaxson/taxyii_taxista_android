package br.taxyii.taxista.activity;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Toast;
import br.taxyii.library.adapter.ListCarroAdper;
import br.taxyii.library.dao.UsuarioDAO;
import br.taxyii.library.faces.ITransacao;
import br.taxyii.library.model.TaxiModel;
import br.taxyii.library.model.UsuarioModel;
import br.taxyii.library.task.DefaultTask;
import br.taxyii.library.utils.AndroidUtils;
import br.taxyii.library.utils.GcmUtils;
import br.taxyii.taxista.R;

import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.ActionMode.Callback;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class ListCarroActivity extends SherlockListActivity implements
		ITransacao {
	private int ADD = 1;
	private String response = null;
	private ListCarroAdper taxiAdapter;
	private ArrayList<TaxiModel> veiculos;
	private DefaultTask task;
	private ActionMode mMode;
	private TaxiModel taxi;
	private int EDIT = 1;
	private int REMOVE = 2;
	private int ADD_ACESSORIO = 3;
	private boolean conexao;
	private Tremove trans_remove;
	private UsuarioModel usuario;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		
		usuario = new UsuarioDAO(getBaseContext()).get();
		
		taxi = new TaxiModel();
		
		ListCarroAdper l = new ListCarroAdper(getBaseContext(),new ArrayList<TaxiModel>());

		setListAdapter(l);

		conexao = AndroidUtils.checkConnection(this);

		if (conexao) {
			task = new DefaultTask(this, this, getString(R.string.pgTitle),getString(R.string.pgMensagemBuscar),0);
			task.execute();
		} else {
			AndroidUtils.showSimpleToast(this, R.string.error_conexao,Toast.LENGTH_SHORT);
			finish();
		}

		getListView().setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,int position, long id) {
				taxi = (TaxiModel) taxiAdapter.getItem(position);
				mMode = startActionMode(new AnActionModeOfEpicProportions());
				return false;
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		menu.add(0, ADD, 0, "Adicionar")
				.setIcon(android.R.drawable.ic_menu_add)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		return true;
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		conexao = AndroidUtils.checkConnection(this);
		if (!conexao) {
			AndroidUtils.showSimpleToast(this, R.string.error_conexao,Toast.LENGTH_SHORT);
			finish();
		}
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();

		if (id == ADD) {
			startActivity(new Intent("CRIAR_TAXI"));
		}
		if (id == android.R.id.home) {
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}

		return super.onMenuItemSelected(featureId, item);

	}

	@Override
	public void executar() throws Exception {
		taxi.setUrl("listartaxi/" + usuario.getId(), GcmUtils.SENDER_ID_MOTORISTA);
		response = taxi.requisicaoHttp("get");
	}

	@SuppressWarnings("unchecked")
	@Override
	public void atualizarView() {
		if (response != null) {
			try {
				JSONObject json = new JSONObject(response);
			
				if (!json.has("error")) {

					veiculos = (ArrayList<TaxiModel>) new TaxiModel().toCollectionObject(new JSONArray(json.getString("data")));
					taxiAdapter = new ListCarroAdper(getBaseContext(), veiculos);
					setListAdapter(taxiAdapter);
				}
			
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private class AnActionModeOfEpicProportions implements Callback {
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			// Used to put dark icons on light action bar

			menu.add(0, EDIT, 0, "Editar").setIcon(R.drawable.ic_action_edit).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
			menu.add(0, REMOVE, 0, "Remover").setIcon(R.drawable.ic_action_remove).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			int id = item.getItemId();

			if (id == EDIT) {
				Intent i = new Intent("CRIAR_TAXI");
				i.putExtra("taxi", taxi.toJson());
				startActivity(i);
			}
			if (id == REMOVE) {
				AndroidUtils.showAlertDialogConfirm(ListCarroActivity.this,"Confirmação.", "Deseja Realmente excluir este item?",new DefaultTask(ListCarroActivity.this, new Tremove()));
			}

			mMode.finish();
			
			return true;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
		}
	}

	private class Tremove implements ITransacao {

		@Override
		public void executar() throws Exception {
			taxi.setUrl("removertaxi/" + taxi.getId(), GcmUtils.SENDER_ID_MOTORISTA);
			response = taxi.requisicaoHttp("get");
		}

		@Override
		public void atualizarView() {
			if (response != null) {
				veiculos.remove(taxi);
				getListView().setAdapter(taxiAdapter);
				AndroidUtils.showSimpleToast(ListCarroActivity.this,R.string.mensagem_sucesso, Toast.LENGTH_SHORT);
			}
		}

	}

}
