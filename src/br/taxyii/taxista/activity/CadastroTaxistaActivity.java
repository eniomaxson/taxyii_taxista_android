package br.taxyii.taxista.activity;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import br.taxyii.library.fragment.CadastroTaxistaFragment;
import br.taxyii.taxista.R;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

public class CadastroTaxistaActivity extends SherlockFragmentActivity {
	
	private CadastroTaxistaFragment fragmentTaxista;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cadastro_taxista);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		
		fragmentTaxista = (CadastroTaxistaFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentCadastroTaxista);
		
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		
		if (id == android.R.id.home) {
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
}
