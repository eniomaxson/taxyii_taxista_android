package br.taxyii.taxista.activity;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import br.taxyii.library.fragment.CadastroTaxiFragment;
import br.taxyii.taxista.R;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

public class CadastroTaxiActivity extends SherlockFragmentActivity {
	
	private CadastroTaxiFragment view;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_cadastro_taxi);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		getSupportActionBar().setHomeButtonEnabled(true);
		
		view = (CadastroTaxiFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentCadastroTaxi);		
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();

		if (id == android.R.id.home) {
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}

		return super.onMenuItemSelected(featureId, item);

	}
}
