package br.taxyii.taxista.activity;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import br.taxyii.library.fragment.ListaCorridaFragment;
import br.taxyii.taxista.R;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

public class ListCorrida extends SherlockFragmentActivity {
	private ListaCorridaFragment lista_corrida;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_corrida);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);

		lista_corrida = (ListaCorridaFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentListCorrida);
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();

		if (id == android.R.id.home) {
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}

		return super.onMenuItemSelected(featureId, item);

	}

}
