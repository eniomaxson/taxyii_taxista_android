package br.taxyii.taxista.activity;

import java.io.IOException;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import br.taxyii.library.dao.UsuarioDAO;
import br.taxyii.library.model.UsuarioModel;
import br.taxyii.library.utils.AndroidUtils;
import br.taxyii.library.utils.GcmUtils;
import br.taxyii.taxista.R;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;

public class HomeActivity extends SherlockActivity implements Runnable {
	private int DOCS = 1;
	private int TAXI = 2;
	private int FORMA_PAGAMETO = 3;
	private int PERFIL = 4;
	private int CONFIG = 5;
	private int SOBRE = 6;
	private int LISTAR_CORRIDA = 8;
	private int SAIR = 7;

	private Button doc;
	private Button taxi;
	private Button forma_pagemento;
	private Button perfil;
	private Button config;
	private Button sobre;
	private String response;

	public boolean gcm_enviado = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		doc = (Button) findViewById(R.id.btn_doc);
		sobre = (Button) findViewById(R.id.btn_sobre);
		taxi = (Button) findViewById(R.id.btn_taxi);
		forma_pagemento = (Button) findViewById(R.id.btn_forma_pagamento);
		perfil = (Button) findViewById(R.id.btn_perfil);
		
		doc.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent("CRIAR_TAXISTA"));
			}
		});
		
		perfil.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent("TAXISTA_PERFIL"));
			}
		});
		
		sobre.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showAlertSobre();
			}
		});

		taxi.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent("LISTAR_TAXI"));
			}
		});

		forma_pagemento.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent("ADD_FRM_PGT"));
			}
		});
		//startService(new Intent("ENVIAR_LOCALIZACAO"));
		new Thread(this).start();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		SubMenu subMenu1 = menu.addSubMenu("Menu");
		subMenu1.add(0, DOCS, 0, "Documentos");
		subMenu1.add(0, TAXI, 0, "Taxi");
		subMenu1.add(0, FORMA_PAGAMETO, 0, "Formas de Pagamento");
		subMenu1.add(0, PERFIL, 0, "Perfil");
		subMenu1.add(0, LISTAR_CORRIDA, 0, "Relat�rio de corridas");
		subMenu1.add(0, SOBRE, 0, "Sobre");
		subMenu1.add(0, SAIR, 0, "Sair");

		MenuItem subMenu1Item = subMenu1.getItem();
		subMenu1Item.setIcon(R.drawable.ic_drawer);

		subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		// TODO Auto-generated method stub
		int opcao = item.getItemId();

		if (DOCS == opcao) {
			startActivity(new Intent("CRIAR_TAXISTA"));
		}

		if (TAXI == opcao) {
			startActivity(new Intent("LISTAR_TAXI"));
		}

		if (SOBRE == opcao) {
			showAlertSobre();
		}

		if (FORMA_PAGAMETO == opcao) {
			startActivity(new Intent("ADD_FRM_PGT"));
		}
		
		if(LISTAR_CORRIDA == opcao){
			startActivity(new Intent("LISTAR_CORRIDA"));
		}
		
		if (SAIR == opcao) {
			UsuarioModel usuario = new UsuarioDAO(getBaseContext()).get();
			new UsuarioDAO(getBaseContext()).Remover(usuario.getId());
			finish();
		}
		return super.onMenuItemSelected(featureId, item);
	}

	public void showAlertSobre() {
		AndroidUtils.showAlertDialogComIntencao(this, "Sobre","Trabalho Conclus�o do Curso Sistemas de Informa��o", null);
	}

	@Override
	public void run() {
		while (gcm_enviado) {
			String enviado = new UsuarioModel().gcmJaEnviado(getBaseContext());

			if (enviado == null) {
				try {
					new GcmUtils(getBaseContext()).register(GcmUtils.SENDER_ID_MOTORISTA);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (enviado.equals("N")) {
				new UsuarioModel().gcmRegister(getBaseContext());
				gcm_enviado = true;
			}
		}
	}

}
