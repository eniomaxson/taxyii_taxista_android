package br.taxyii.taxista.activity;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.NavUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import br.taxyii.library.model.CidadeModel;
import br.taxyii.library.model.UsuarioModel;
import br.taxyii.library.task.DefaultTask;
import br.taxyii.library.utils.AndroidUtils;
import br.taxyii.library.utils.IOUtils;
import br.taxyii.library.utils.ImageUtils;
import br.taxyii.library.utils.SDCardUtils;
import br.taxyii.taxista.R;

import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class CriarUsuarioActivity extends
		br.taxyii.library.activity.CriarUsuarioActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_criar_usuario);

		loadInput();

		conectado = AndroidUtils.checkConnection(this);

		if (!conectado) {
			AndroidUtils.showSimpleToast(this, R.string.error_conexao,
					Toast.LENGTH_SHORT);
			finish();
		}

		foto.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mMode = startActionMode(new AnActionModeOfEpicProportions());
			}
		});

	}

	public void loadInput() {
		ArrayList<String> sexos = new ArrayList<String>();

		sexos.add("Masculino");
		sexos.add("Feminino");

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, sexos);

		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		sexo = (Spinner) findViewById(R.id.sexo);
		nome = (EditText) findViewById(R.id.nome);
		sobrenome = (EditText) findViewById(R.id.sobrenome);
		cep = (EditText) findViewById(R.id.cep);
		celular1 = (EditText) findViewById(R.id.celular1);
		celular2 = (EditText) findViewById(R.id.celular2);
		cidade = (EditText) findViewById(R.id.cidade);
		email = (EditText) findViewById(R.id.email);
		senha = (EditText) findViewById(R.id.senha);
		foto = (ImageView) findViewById(R.id.profile);
		sexo.setAdapter(adapter);
		setListenerCep();
	}

	@Override
	public void createUsuario() {
		// TODO Auto-generated method stub
		super.createUsuario();

		setInputErros();

		conectado = AndroidUtils.checkConnection(this);

		if (validado && conectado) {
			usuario = new UsuarioModel();
			usuario.setCelular(celular);
			usuario.setCidade(new CidadeModel(cidade.getText().toString()));
			usuario.setNome(nome.getText().toString());
			usuario.setSobrenome(sobrenome.getText().toString());
			usuario.setEmail(email.getText().toString());
			usuario.setSenha(senha.getText().toString());
			usuario.setSexo(sexo.getSelectedItem().toString());
			usuario.setBloqueio("1");
			// usuario.setFoto(null);

			criarUsuarioTask = new DefaultTask(this, this,getString(R.string.progressTituloCadastroUsuario),getString(R.string.progressMessagemCadastroUsuario), 0);
			criarUsuarioTask.execute();

		} else {
			validado = true;
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, SALVAR, 0, "Salvar")
				.setIcon(R.drawable.abs__ic_cab_done_holo_dark)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int menu_id = item.getItemId();

		if (menu_id == android.R.id.home) {
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}

		if (menu_id == SALVAR) {
			createUsuario();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK) {
			if (requestCode == REQUEST_CAMERA) {
				Bitmap bitmap = ImageUtils.getResizedImage(Uri
						.fromFile(SDCardUtils
								.getSdCardFile(imgDirName, imgName)), 210, 230);
				foto.setImageBitmap(bitmap);
			}
			if (requestCode == REQUEST_GALERIA) {

				Uri selectedImage = data.getData();
				InputStream imageStream;
				try {
					imageStream = getContentResolver().openInputStream(
							selectedImage);

					file = SDCardUtils.getSdCardFile(imgDirName, imgName);

					SDCardUtils.writeToSdCard(file,
							IOUtils.toBytes(imageStream));

					Bitmap bitmap = ImageUtils.getResizedImage(
							Uri.fromFile(file), 210, 230);

					foto.setImageBitmap(bitmap);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	private final class AnActionModeOfEpicProportions implements
			ActionMode.Callback {
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			// Used to put dark icons on light action bar
			menu.add("camera").setIcon(android.R.drawable.ic_menu_camera)
					.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
			menu.add("galery").setIcon(android.R.drawable.ic_menu_gallery)
					.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			file = SDCardUtils.getSdCardFile(imgDirName, imgName);

			if (item.toString().equals("camera")) {

				Intent i = new Intent("android.media.action.IMAGE_CAPTURE");

				i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));

				startActivityForResult(i, REQUEST_CAMERA);
			}

			if (item.toString().equals("galery")) {

				Intent i = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

				startActivityForResult(i, REQUEST_GALERIA);
			}
			return true;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
		}
	}

}
