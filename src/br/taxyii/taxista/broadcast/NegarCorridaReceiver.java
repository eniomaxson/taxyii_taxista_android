package br.taxyii.taxista.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NegarCorridaReceiver extends BroadcastReceiver{

	@Override
	public void onReceive(Context context, Intent intent) {
		context.startService(new Intent("NEGAR_CORRIDA"));
	}

}
