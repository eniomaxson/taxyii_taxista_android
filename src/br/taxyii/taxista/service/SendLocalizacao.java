package br.taxyii.taxista.service;

import android.app.IntentService;
import android.content.Intent;
import br.taxyii.library.dao.UsuarioDAO;
import br.taxyii.library.model.LocalizacaoModel;
import br.taxyii.library.model.TaxistaModel;
import br.taxyii.library.model.UsuarioModel;
import br.taxyii.library.utils.GcmUtils;
import br.taxyii.library.utils.SharedPreferencesUtils;


public class SendLocalizacao extends IntentService{
	private TaxistaModel taxista;
	private LocalizacaoModel localizacao;
	private UsuarioModel usuario;
	private SharedPreferencesUtils pref;
	private String status;
	public SendLocalizacao() {
		super("enviarLocalizacao");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		
		pref = new SharedPreferencesUtils(getBaseContext(), SharedPreferencesUtils.STATUS_PREFERENCES);
		
		status = pref.getPreferences(SharedPreferencesUtils.STATUS_TAXISTA, null); 
		
		status = status == null ? "D" : status;
		
		usuario = new UsuarioDAO(getBaseContext()).get();
		
		taxista = new TaxistaModel();
		
		while(true){
			
			if(status.equals("D")){
				
			taxista.setUrl("atualizarposicao/"+ usuario.getId() + "/" + "-5.806267,-35.221923", GcmUtils.SENDER_ID_MOTORISTA);
				try {
					taxista.requisicaoHttp("get");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}		
	}
}
