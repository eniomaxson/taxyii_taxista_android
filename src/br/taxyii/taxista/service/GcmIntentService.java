package br.taxyii.taxista.service;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import br.taxyii.library.utils.AndroidUtils;
import br.taxyii.library.utils.NotificationUtils;
import br.taxyii.library.utils.SharedPreferencesUtils;
import br.taxyii.taxista.R;

import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GcmIntentService extends IntentService {

	private static final String TAG = "GCMService";
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;

	
	public GcmIntentService() {
		super("GCMIntentService");
	}


	@Override
	protected void onHandleIntent(Intent intent) {
		   Bundle extras = intent.getExtras();
		   
	        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
	       
	        String messageType = gcm.getMessageType(intent);

	        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
	            /*
	             * Filter messages based on message type. Since it is likely that GCM will be
	             * extended in the future with new message types, just ignore any message types you're
	             * not interested in, or that you don't recognize.
	             */
	            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
	                
	            	//sendNotification("Send error: " + extras.toString());
	            
	            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
	                //sendNotification("Deleted messages on server: " + extras.toString());
	            // If it's a regular GCM message, do some work.
	            
	            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
	                // This loop represents the service doing some work.
	                for (int i = 0; i < 5; i++) {
	                    Log.i(TAG, "Working... " + (i + 1)
	                            + "/5 @ " + SystemClock.elapsedRealtime());
	                    try {
	                        Thread.sleep(5000);
	                    } catch (InterruptedException e) {
	                    }
	                }
	                //Log.i(TAG, "Completed work @ " + SystemClock.elapsedRealtime());
	                // Post notification of received message.
	                JSONObject json;
	                String tipo = null;
	                
	                try {
	    				json = new JSONObject(extras.getString("msg"));
	    				
	    				tipo = json.getString("tipo");
	    				
	    				if(tipo.equals("solicitacao_corrida"))
	    				{
	    					notificarCorrida(extras, json);
	    				}
	    			} catch (JSONException e) {
	    				e.printStackTrace();
	    			}
	            }
	        }
	        // Release the wake lock provided by the WakefulBroadcastReceiver.
	        WakefulBroadcastReceiver.completeWakefulIntent(intent);
	    }

		private void notificarCorrida(Bundle extras, JSONObject json){
			String msg = getString(R.string.notificationNovoPedidoPassageiro);
			Intent i = new Intent("ACEITAR_PEDIDO");
			try {
				json = new JSONObject(extras.getString("msg"));
				msg = "Sr(a) " +json.getString("passageiro_nome") + " " + msg;
                
				SharedPreferencesUtils utils = new SharedPreferencesUtils(getBaseContext(), SharedPreferencesUtils.CORRIDA_PREFERENCES);
				
				utils.setPreferences(SharedPreferencesUtils.CORRIDA_ATUAL, extras.getString("msg"));
                
				sendNotification(msg, i);
				
				//Executar ap�s 30 segundos a fun��o negar corrida.
				AndroidUtils.setAlarm(getBaseContext(), 30, new Intent("EXE_NEGAR_CORRIDA"),false);
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
	    private void sendNotification(String msg, Intent intent) {
	        NotificationUtils.create(getBaseContext(),getString(R.string.notificationNovoPedido), getString(R.string.notificationNovoPedido), msg, R.drawable.thumb, R.string.idNotificacaoAceitarCorrida, intent); 
	    }	
}