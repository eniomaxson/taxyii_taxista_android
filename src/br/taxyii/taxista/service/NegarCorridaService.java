package br.taxyii.taxista.service;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import br.taxyii.library.R;
import br.taxyii.library.dao.UsuarioDAO;
import br.taxyii.library.model.CorridaModel;
import br.taxyii.library.model.UsuarioModel;
import br.taxyii.library.utils.GcmUtils;
import br.taxyii.library.utils.SharedPreferencesUtils;

public class NegarCorridaService extends IntentService {

	private UsuarioModel usuario;
	private CorridaModel corrida;
	private JSONObject  json;

	public NegarCorridaService() {
		super("negarCorrida");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		SharedPreferencesUtils utils = new SharedPreferencesUtils(getBaseContext(), SharedPreferencesUtils.CORRIDA_PREFERENCES);
		usuario = new UsuarioDAO(getBaseContext()).get();
		try {
			
			String dado_corrida = utils.getPreferences(SharedPreferencesUtils.CORRIDA_ATUAL, null);			
			
			json = new JSONObject(dado_corrida);			
			
			if(!json.has("STATUS"))
			{	
				corrida = (CorridaModel) new CorridaModel().toObject(json);
				// negarcorrida/<corrida_id>/<usuario_id>/<device_id>
				usuario.setUrl("negarcorrida/"+ corrida.getId() + "/" + usuario.getId(), GcmUtils.SENDER_ID_MOTORISTA);
				usuario.requisicaoHttp("get");	
				
				NotificationManager nm = (NotificationManager) getBaseContext().getSystemService(Context.NOTIFICATION_SERVICE);
				nm.cancel(R.string.idNotificacaoAceitarCorrida);
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
